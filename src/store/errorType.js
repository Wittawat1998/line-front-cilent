export const ErrorType = {
  "ERR_BAD_REQUEST": 401,
  "ERR_BAD_RESPONSE": 500
}

// [
  // server : unauthorized
//   {
//     status: 401,
//     code: "ERR_BAD_REQUEST",
//     message: "Request failed with status code 401"
//   },
  // server : badImplementation
//   {
//     status: 500,
//     code: "ERR_BAD_RESPONSE",
//     message: "Request failed with status code 500"
//   }
// ]
