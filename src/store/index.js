import { configureStore } from '@reduxjs/toolkit'
import profile from "./slices/profile.slice";

export const store = configureStore({
  reducer: {
    profile,
  },
})
