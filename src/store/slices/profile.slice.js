import { createSlice, createAsyncThunk } from '@reduxjs/toolkit'
import ProfileService from "../../services/profile.service";
import { ErrorType } from "../errorType";

// export const getCountry = createAsyncThunk(
//   'profile/getCountry',
//   async () => {
//     const response = await ProfileService.getCountry()
//     return response.data
//   }
// )

export const getStates = createAsyncThunk(
  'profile/getStates',
  async (countryCode) => {
    const response = await ProfileService.getStates(countryCode)
    return response.data
  }
)

export const getBrand = createAsyncThunk(
  'profile/getBrand',
  async () => {
    const response = await ProfileService.getBrand()
    return response.data
  }
)

export const getModel = createAsyncThunk(
  'profile/getModel',
  async (brandId) => {
    const response = await ProfileService.getModel(brandId)
    return response.data
  }
)

export const getColor = createAsyncThunk(
  'profile/getColor',
  async () => {
    const response = await ProfileService.getColor()
    return response.data
  }
)

export const getEvConnector = createAsyncThunk(
  'profile/getEvConnector',
  async () => {
    const response = await ProfileService.getEvConnector()
    return response.data
  }
)

export const getProfile = createAsyncThunk(
  'profile/getProfile',
  async () => {
    const response = await ProfileService.getProfile()
    return response.data
  }
)

// The initial state for the reducer
const initialState = {
  // countrys: [],
  states: [],
  brands: [],
  models: [],
  colors: [],
  evConnectors: [],
  vehicles: [],
  currentVehicle: {
    id: '',
    licensePlate: '',
    stateName: {},
    brandName: '',
    modelName: '',
    casisCar: '',
    color: '',
    connector: '',
    modelImage: '',
  },
  vehicleId: '', // ใช้ตอน reconnect กลับไปหน้า charge
  profileInfo: {
    countryCode: '',
    countryId: '',
  },
  errorStatus: 0,
  isEdit : false
}

export const profileSlice = createSlice({
  name: 'profile', // A name, used in action types
  initialState,
  reducers: {
    setCurrentVehicle: (state, action) => {
      state.currentVehicle = action.payload
      state.vehicleId = action.payload.id
    },
    setVehicles: (state, action) => {
      state.vehicles.push(action.payload)
    },
    setErrorStatus: (state, action) => {
      state.errorStatus = action.payload
    },
    setVeicleId: (state, action) => {
      state.vehicleId = action.payload
    },
    setIsEdit: (state, action) => {
      state.isEdit = action.payload
    },
  },
  extraReducers: (builder) => {
    // Add reducers for additional action types here, and handle loading state as needed
    // builder.addCase(getCountry.fulfilled, (state, action) => {
    //   state.countrys = action.payload.countrys
    // })
    builder
      .addCase(getStates.fulfilled, (state, action) => {
        state.states = action.payload.states
      })
      .addCase(getStates.rejected, (state, action) => {
        const { code } = action.error
        state.errorStatus = ErrorType[code]
      })
    builder
      .addCase(getBrand.fulfilled, (state, action) => {
        state.brands = action.payload.brands
      })
      .addCase(getBrand.rejected, (state, action) => {
        const { code } = action.error
        state.errorStatus = ErrorType[code]
      })
    builder
      .addCase(getModel.fulfilled, (state, action) => {
        state.models = action.payload.models.vehicleModel
      })
      .addCase(getModel.rejected, (state, action) => {
        const { code } = action.error
        state.errorStatus = ErrorType[code]
      })
    builder
      .addCase(getColor.fulfilled, (state, action) => {
        state.colors = action.payload.colors
      })
      .addCase(getColor.rejected, (state, action) => {
        const { code } = action.error
        state.errorStatus = ErrorType[code]
      })
    builder
      .addCase(getEvConnector.fulfilled, (state, action) => {
        state.evConnectors = action.payload.evConnectors
      })
      .addCase(getEvConnector.rejected, (state, action) => {
        const { code } = action.error
        state.errorStatus = ErrorType[code]
      })
    builder
      .addCase(getProfile.fulfilled, (state, action) => {
        state.profileInfo = action.payload.profileInfo
        state.vehicles = action.payload.vehicles
      })
      .addCase(getProfile.rejected, (state, action) => {
        const { code } = action.error
        state.errorStatus = ErrorType[code]
      })
  },
})

export const {
  setCurrentVehicle,
  setVehicles,
  setErrorStatus,
  setVeicleId,
  setIsEdit,
} = profileSlice.actions

export default profileSlice.reducer
