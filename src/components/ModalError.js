import * as React from 'react';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Modal from '@mui/material/Modal';
import { Link } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import iconNoti from "../assets/icons/icon_noti.svg";

function ModalError(props) {
  const { t } = useTranslation();

  /*
    --- Error from api
    400 = request payload invalid
    401 = authen fail
    500 = internal error from server
    41001 = charge point available
    51001 = ocpp error

    --- Error frontend
    99 = timeout
    98 = qrcode : invalid alias
    97 = qrcode : device unready
  */
  const {
    open,
    errorCode = 500,
    linkTo = '/home',
    replace = true,
  } = props
  console.log(errorCode)
  return (
    <Modal
      open={open}
    // onClose={handleClose}
    >
      <Box className="modal">
      <img src={iconNoti} alt="icon-error" style={{ width: "50px", marginBottom: "30px" , marginTop: "20px"}}/>
        <div className="modal-title">
          {t(`error.${errorCode}`)}
        </div>

        <br />
        <div className="modal-btn-qrcode-error">
        <Link to={linkTo} replace={replace}>
          <Button variant="text" >
            {t('modal.btn_confirm')}
          </Button>
        </Link>
        </div>
      </Box>
    </Modal>
  );
}

export default ModalError