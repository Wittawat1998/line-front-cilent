import * as React from 'react';
import { withTranslation } from 'react-i18next';
import ModalError from '../components/ModalError';

class Home extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      toChargPoint: false,
      errorCode: 98,
    };

  }


  render() {
    const {
      toChargPoint,
      errorCode,
    } = this.state
    return (
      <div >

        <ModalError
          open={toChargPoint}
          errorCode={errorCode}
          linkTo='/home'
        />
        <p className='home-value'>brandName</p>
      </div>
    );
  }
}


export default (withTranslation()(Home))
