import Constants from "../configs/constants";

export function getMobileOperatingSystem() {
  var userAgent = navigator.userAgent || navigator.vendor || window.opera;

  // Windows Phone must come first because its UA also contains "Android"
  if (/windows phone/i.test(userAgent)) {
    return "Windows Phone";
  }

  if (/android/i.test(userAgent)) {
    return "Android";
  }

  // iOS detection from: http://stackoverflow.com/a/9039885/177710
  if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) {
    return "iOS";
  }

  return "unknown";
}

export function splitAliasConnector(strData) {
  if (strData === '' || strData.length < Constants.aliasLength) {
    return {
      alias: '',
      connectorId: ''
    }
  }

  const alias = strData.substr(0, Constants.aliasLength)
  const connectorId = strData.substr(Constants.aliasLength, (strData.length - Constants.aliasLength))

  return {
    alias,
    connectorId
  }
}

export function getIdFileFromPath(strData) {
  const fullName = strData.split("/")[2];
  const fileId = fullName.split(".")[1];

  return fileId
}
