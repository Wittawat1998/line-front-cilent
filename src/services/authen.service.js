
import API from './config';

const authen = (payload) => {
  return API.post('/authen', payload);
}

const AuthenService = {
  authen
}

export default AuthenService
