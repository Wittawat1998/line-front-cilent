import axios from 'axios';
import Cookies from 'js-cookie'
import Constants from "../configs/constants";

const token = Cookies.get('token') || ''

export default axios.create({
  baseURL: Constants.fileUrl,
  headers: {
    Authorization: `Bearer ${token}`
  },
  withCredentials: true // allow cookie in header
});