import fileAPI from './configFile';

const uploadFile = (data) => {
  return fileAPI.post('/', data);
}

const deleteFile = (fileId) => {
  return fileAPI.delete('/' + fileId);
}


const FileService = {
  uploadFile,
  deleteFile,
}

export default FileService
