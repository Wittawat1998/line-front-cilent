import axios from 'axios';
import Cookies from 'js-cookie'
import Constants from "../configs/constants";

const token = Cookies.get('token') || ''
console.log('Token [cookie] for auth API: ', token);

export default axios.create({
  baseURL: Constants.api,
  headers: {
    Authorization: `Bearer ${token}`
  },
  withCredentials: true // allow cookie in header
});