import API from './config';

const getCountry = () => {
  return API.get('/country');
}

const createVehicle = (data) => {
  return API.post('/vehicle', data);
}

const updateVehicle = (data) => {
  return API.patch('/vehicle', data);
}

const ProfileService = {
  getCountry,
  createVehicle,
  updateVehicle
}

export default ProfileService
