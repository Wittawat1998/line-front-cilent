import React from 'react'
import { Route, Routes, Navigate } from 'react-router-dom'
import Cookies from 'js-cookie'
// Pages ---
import Home from "./pages/home";


const Router = () => (
  <Routes>
    {/* === Private Route === */}
    <Route path="/" element={<Home /> } />

    {/* === Public Route === */}
    {/* <Route path="/" element={<Home />} /> */}
  </Routes>
)

export default Router
