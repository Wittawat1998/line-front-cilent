import i18n from 'i18next'
import { initReactI18next  } from 'react-i18next'
import Backend from 'i18next-xhr-backend'
import LanguageDetector from 'i18next-browser-languagedetector'

i18n
  .use(Backend)
  .use(LanguageDetector)
  .use(initReactI18next )
  .init({
    whitelist: ['th', 'en'],
    fallbackLng: 'en',
    ns: ['translations'],
    defaultNS: 'translations',
    detection: {
      order: ['localStorage', 'htmlTag', 'querystring', 'cookie', 'navigator', 'path', 'subdomain']
    },
    preload: ['th', 'en'],
    debug: false,
    react: {
      wait: true,
      nsMode: 'fallback'
    },
    interpolation: {
      escapeValue: false
    }
  })

export default i18n
