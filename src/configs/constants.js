const env = process.env.REACT_APP_ENV || 'local'

// Disable log on production
// if (env === 'production') {
//     console.log = () => { }
//     console.error = () => { }
//     console.debug = () => { }
// }
console.log("ENV: ", process.env);

/*
    --- Config detail ---
    timeOutConnect: default 5m  => 300000 ms = 1000 x 60 x 5(minute)
*/

const local = {
    baseUrl: 'http://localhost',
    api: 'http://localhost:8002/api/app',
    webSocket: 'ws://localhost:8002/app',
    fileUrl: 'http://localhost:8008',
    timeOutConnect: 300000,
    aliasLength: 8,
    reconnectWS: 3000,
}

const development = {
    baseUrl: 'https://ev-app.tcctapp.ga',
    api: 'https://ev-appws.tcctapp.ga/api/app',
    webSocket: 'wss://ev-appws.tcctapp.ga/app',
    fileUrl: 'https://ev-file.tcctapp.ga',
    timeOutConnect: 300000,
    aliasLength: 8,
    reconnectWS: 3000,
}

const production = {
    baseUrl: 'https://evapp.thaibevapp.com',
    api: 'https://evappws.thaibevapp.com/api/app',
    webSocket: 'wss://evappws.thaibevapp.com/app',
    fileUrl: 'https://evfile.thaibevapp.com',
    timeOutConnect: 300000,
    aliasLength: 8,
    reconnectWS: 3000,
}

let config = null
if (env.trim() === 'local') config = local
else if (env.trim() === 'development') config = development
else if (env.trim() === 'production') config = production

console.log("config: ", config);

module.exports = config;
